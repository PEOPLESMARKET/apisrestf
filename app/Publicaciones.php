<?php
    namespace App;
    use Illuminate\Database\Eloquent\Model;

    class Publicaciones extends Model
    {
        protected $table = 'Publicaciones';
        protected $primaryKey = 'id';
        protected $fillable= ['id','producto','marca','color','celular','metodoPago','descripcion','precio','tipoProducto','imagenes','estado'];
    }

?>
