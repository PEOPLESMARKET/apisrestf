<?php

return [
    'defaults' => [
        'guard' => 'api',
        'passwords' => 'Usuarios',
    ],

    'guards' => [
        'api' => [
            'driver' => 'jwt',
            'provider' => 'Usuarios',
        ],
    ],

    'providers' => [
        'Usuarios' => [
            'driver' => 'eloquent',
            'model' => \App\Usuarios::class
        ]
    ]
];
