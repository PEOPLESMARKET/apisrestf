<?php

$router->get('/usuarios','UsuariosController@index');
$router->get('/usuarios/{id}','UsuariosController@getUsuarios');
$router->get('/usuario/{user}','UsuariosController@validUser');
$router->post('/login','UsuariosController@login');
$router->post('/usuarios','UsuariosController@createUsuarios');
$router->put('/usuarios/{id}','UsuariosController@updateUsuarios');
$router->delete('/usuarios/{id}','UsuariosController@destroyUsuarios');
$router->put('/pass/{id}','UsuariosController@cambio');
$router->get('/publicaciones','PublicacionesController@index');
$router->get('/publicaciones/{id}','PublicacionesController@getPub');
$router->post('/publicaciones','PublicacionesController@createPublicacion');
$router->get('/categorias','CategoriasController@index');
$router->post('/categorias','CategoriasController@createCategoria');
$router->put('/publicaciones/{id}','PublicacionesController@inhabilitar');
$router->put('/publicacion/{id}','PublicacionesController@actualizarPublicacion');





$router->get('/', function () use ($router) {
    return $router->app->version();
});
